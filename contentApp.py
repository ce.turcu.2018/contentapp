# esta es la clase derivada que tendrá que heredar de la madre y servir el diccionario

from web import webApp


class contentApp (webApp):

    content = {'/': 'Root page',
               '/page1': 'This is a page',
               '/page2': 'This is the final page'
               }

    def parse(self, request):
        return request.split(' ', 2)[1]

    def process(self, resourceName):
        if resourceName in self.content.keys():
            httpCode = "200 OK"
            htmlBody = "<html><body>" + self.content[resourceName] \
                + "</body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "Resource Not Found"
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 8080)
